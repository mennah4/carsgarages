import Vue from 'vue'
import Router from 'vue-router'

import Auth from '@/components/pages/Auth'
import Garages from '@/components/pages/Garages'

import Cars from '@/components/pages/Cars'
import CarDetails from '@/components/pages/CarDetails'
import Create from '@/components/pages/Create'
import Edit from '@/components/pages/Edit'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Garages',
      component: Garages
    },
    {
      path: '/cars',
      name: 'Cars',
      component: Cars
    },
    {
      path: '/cardetails/:car_id',
      name: 'cardetails',
      component: CarDetails
    },
    {
      path: '/create',
      name: 'create',
      component: Create
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: Edit
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth
    }
  ]
})
