import axios from 'axios'
import Router from 'vue-router'
import ApiService from '@/services/APIServices'
import swal from 'sweetalert2';

import { FETCH_AUTH
      } from './actionsTypes'
import { FETCH_START,
         FETCH_END,
         SET_TOKEN,
         SET_ERROR
       } from './mutationsTypes'
import Vue from 'vue';
const state = {

    token: "",
    errors: {},
    loading: false

}
const getters = {
      token (state) {
        return state.token;
      },
      isLoading (state) {
        return state.loading;
      }
}
const actions = {
    [FETCH_AUTH] : function (context, payload) {
        context.commit(FETCH_START)
        return axios.post('http://localhost:8000/auth/', payload).then(res => {
                this.$session.start();
                this.$session.set('token', res.data.token);
                context.commit(SET_TOKEN, res.data.token);
                context.commit(FETCH_END)
                console.log(res.data.token)
                router.push('/');
              }).catch(e => {
                context.commit(SET_ERROR, e)
                swal({
                  type: 'warning',
                  title: 'Error',
                  text: 'Wrong username or password',
                  showConfirmButton:false,
                  showCloseButton:false,
                  timer:3000
                })
              })
      }
}
const mutations = {
    [FETCH_START] (state) {
        state.loading = true
      },
      [FETCH_END] (state) {
        state.loading = false
      },
      [SET_TOKEN] (state, pToken) {
        state.token = pToken
        state.errors = {}
      },
      [SET_ERROR] (state , errors) {
        state.errors = errors
        console.log(errors)
      }
}
export default {
  state,
  getters,
  actions,
  mutations
}