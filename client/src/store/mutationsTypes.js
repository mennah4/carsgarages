export const FETCH_START = "loadingOn"
export const FETCH_END = "loadingOff"
export const SET_ERROR = "setError"
// related to garages
export const SET_A_GARAGE = "setAGarage"
export const SET_GARAGES = "setGarages"

// related to cars
export const SET_A_CAR = "setACar"
export const SET_CARS = "setCars"

export const SET_TOKEN = "setToken"