import axios from 'axios'

import ApiService from '@/services/APIServices'
import { FETCH_GARAGES,
         FETCH_A_GARAGE
      } from './actionsTypes'
import { FETCH_START,
         FETCH_END,
         SET_GARAGES,
         SET_A_GARAGE,
         SET_ERROR
       } from './mutationsTypes'
import Vue from 'vue';
const state = {

    garages: [],
    garage: {},
    errors: {},
    loading: false

}
const getters = {
    currentGarage (state) {
        return state.garage
      },
      garages (state) {
        return state.garages;
      },
      isLoading (state) {
        return state.loading;
      }
}
const actions = {
    [FETCH_GARAGES] (context, payload) {
        context.commit(FETCH_START)
        console.log("acac")
        return axios
          .get('http://localhost:8000/garages/')
          .then(({data}) => {
            context.commit(SET_GARAGES, data);
            context.commit(FETCH_END)
            console.log("efg")
          })
          .catch(({response}) => {
            context.commit(SET_ERROR, response)
          })
      },
      [FETCH_A_GARAGE] (context, payload) {
        context.commit(FETCH_START)
        const {garage_id} = payload
        return ApiService
          .get(`garage/${garage_id}`)
          .then(({data}) => {
            context.commit(SET_A_GARAGE, data);
            context.commit(FETCH_END)
          })
          .catch(({response}) => {
            context.commit(SET_ERROR, response)
          })
      }
}
const mutations = {
    [FETCH_START] (state) {
        state.loading = true
      },
      [FETCH_END] (state) {
        state.loading = false
      },
      [SET_GARAGES] (state, pGarages) {
        state.garages = pGarages
        state.errors = {}
      },
      [SET_A_GARAGE] (state, pGarage) {
        state.garage = pGarage
        state.errors = {}
      },
      [SET_ERROR] (state , errors) {
        state.errors = errors
      }
}
export default {
  state,
  getters,
  actions,
  mutations
}