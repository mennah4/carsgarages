import axios from 'axios'

import ApiService from '@/services/APIServices'
import { FETCH_CARS,
         FETCH_A_CAR
      } from './actionsTypes'
import { FETCH_START,
         FETCH_END,
         SET_CARS,
         SET_A_CAR,
         SET_ERROR
       } from './mutationsTypes'
import Vue from 'vue';
const state = {

    cars: [],
    car: {},
    errors: {},
    loading: false

}
const getters = {
    currentCar (state) {
        return state.car
      },
      cars (state) {
        return state.cars;
      },
      isLoading (state) {
        return state.loading;
      }
}
const actions = {
    [FETCH_CARS] (context, payload) {
        context.commit(FETCH_START)
        return axios
          .get('http://localhost:8000/cars/')
          .then(({data}) => {
            context.commit(SET_CARS, data);
            context.commit(FETCH_END)
          })
          .catch(({response}) => {
            context.commit(SET_ERROR, response)
          })
      },
      [FETCH_A_CAR] (context, payload) {
        context.commit(FETCH_START)
        const car_id = payload
        return axios
          .get(`http://localhost:8000/cars/${car_id}`)
          .then(({data}) => {
            context.commit(SET_A_CAR, data);
            console.log(data)
            context.commit(FETCH_END)
          })
          .catch(({response}) => {
            context.commit(SET_ERROR, response)

          })
      }
}
const mutations = {
    [FETCH_START] (state) {
        state.loading = true
      },
      [FETCH_END] (state) {
        state.loading = false
      },
      [SET_CARS] (state, pCars) {
        state.cars = pCars
        state.errors = {}
      },
      [SET_A_CAR] (state, pCar) {
        state.car = pCar
        state.errors = {}
      },
      [SET_ERROR] (state , errors) {
        state.errors = errors
      }
}
export default {
  state,
  getters,
  actions,
  mutations
}