export const FETCH_A_GARAGE = "fetchAGarage"
export const FETCH_GARAGES = "fetchGarages"

export const FETCH_A_CAR = "fetchACar"
export const FETCH_CARS = "fetchCars"

export const FETCH_AUTH = "fetchAuth"
