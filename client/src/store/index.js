import Vue from 'vue'
import Vuex from 'vuex'
import garages from './garages'
import cars from './cars'
import auth from './auth'
Vue.use(Vuex)
export default new Vuex.Store({
  modules: {
    garages,
    cars,
    auth
  }
})